package com.returninfinity.smsmessaging;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SmsMessagingActivity extends Activity
{
	private Button btnSendSms;
	private EditText txtPhoneNo;
	private EditText txtMessage;
	private PendingIntent sentIntent;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		btnSendSms = (Button) findViewById(R.id.btnSendSMS);
		txtPhoneNo = (EditText) findViewById(R.id.txtPhoneNo);
		txtMessage = (EditText) findViewById(R.id.txtMessage);
		
		btnSendSms.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v)
			{
				String phoneNo = txtPhoneNo.getText().toString();
				String message = txtMessage.getText().toString();
				
				if (phoneNo.length() > 0 && message.length() > 0)
					sendSMS(phoneNo, message);
				else
					Toast.makeText(getBaseContext(), "Please enter a phone number and a message", Toast.LENGTH_SHORT).show();
			}
		});
		
		sentIntent = PendingIntent.getActivity(this, 0, new Intent(this, SmsMessagingActivity.class), 0);
	}
	
	/**
	 * Send a SMS message to another device
	 * 
	 * @param phoneNo
	 * @param message
	 */
	private void sendSMS(String phoneNo, String message)
	{
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNo, null, message, sentIntent, null);
	}
}
